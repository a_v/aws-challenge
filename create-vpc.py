#!/usr/bin/env python
import boto3
import sys

###Configuraiton Variables###
aws_acess_key = ''
aws_secret_key = ''
region = 'us-west-2'
subnets = ['192.168.1.0/24']
az = ['us-west-2a']
cidr_block = '192.168.0.0/16'
ami_id = 'ami-1e299d7e'
ec2_key_name = 'MINI-PROJECT'
instance_type = 't2.micro'

client = boto3.client('ec2', aws_access_key_id=aws_acess_key,
                            aws_secret_access_key=aws_secret_key,
                            region_name=region )
ec2 = boto3.resource('ec2', aws_access_key_id=aws_acess_key,
                            aws_secret_access_key=aws_secret_key,
                            region_name=region)


#Checking for vpc 
def vpc_check ():
    response =client.describe_vpcs(
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': [
                'VPC',
            ]
        },
        {
            'Name': 'cidr-block-association.cidr-block',
            'Values': [
                '192.168.0.0/16', #Enter you cidr block here
            ]
        },        
    ]
)
    resp = response['Vpcs']
    if resp:
        print('Found VPC')
        sys.exit(resp)
    
    else:
        print('This VPC is Not here')
    return

#Creating VPC with and Internet gateway with tags# 
def create_vpc (cidr_block):
    vpc = ec2.create_vpc(CidrBlock= cidr_block)
    vpc.create_tags(Tags=[{"Key": "Name", "Value": "VPC"}])
    vpc.wait_until_available()
    client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsSupport = { 'Value': True } )
    client.modify_vpc_attribute( VpcId = vpc.id , EnableDnsHostnames = { 'Value': True } )
    print('Created VPC')
    return vpc
#Creating Internet Gateway
def create_internet_gateway(vpc):
     ig = ec2.create_internet_gateway()
     ec2.create_tags(Tags=[{"Key": "Name", "Value": "VPC_IG"}], Resources=[ig.id])
     vpc.attach_internet_gateway(InternetGatewayId=ig.id)
     print("Created Internet Gateway")
     return ig.id
     
#Creating subnets with tags
def create_subnets_list (subnets, az, vpc):
    for sub, azone in zip(subnets, az,):
        s = ec2.create_subnet(CidrBlock = sub, VpcId = vpc.id, AvailabilityZone = azone )
        ec2.create_tags(Tags=[{"Key": "Name", "Value": "VPC_SUBS"}], Resources=[s.id])
        print('Created Subnet(s): {0}'.format(subnets))
        return s.id

#Updating Route Table 
def update_route_table(ig_id, vpc): 
    for route_table in vpc.route_tables.all():  # There should only be one route table to start with
        route_table.create_route(DestinationCidrBlock='0.0.0.0/0', GatewayId=ig_id)
        ec2.create_tags(Tags=[{"Key": "Name", "Value": "VPC_ROUTE_TABLE"}], Resources=[route_table.id])
        print ('Pointing 0.0.0.0/0 to Created Internet Gateway in Route Table')
        
#creating Security Groups 
def creating_security_groups (vpc): 
    sec_group1 = ec2.create_security_group(
        GroupName='DEFAULT-HTTP', Description='Allow port 80', VpcId=vpc.id)
    sec_group1.authorize_ingress(
         CidrIp='0.0.0.0/0',
        IpProtocol='tcp',
        FromPort=80,
        ToPort=80
    )
    ec2.create_tags(Tags=[{"Key": "Name", "Value": "VPC_PORT_80"}], Resources=[sec_group1.id])
    print('Created HTTP Security Group')

# Create sec group http and ssh Security group
    sec_group2 = ec2.create_security_group(
        GroupName='DEFAULT-SSH', Description='Allow port 22', VpcId=vpc.id)
    sec_group2.authorize_ingress(
        CidrIp='0.0.0.0/0',
        IpProtocol='tcp',
        FromPort=22,
        ToPort=22
    )
    ec2.create_tags(Tags=[{"Key": "Name", "Value": "PORT_22"}], Resources=[sec_group2.id])
    print('Created SSH Security Group')
    return [sec_group1.id, sec_group2.id]

#Checking and Creating Key Pairs
def creating_key_pairs ():
    customEC2Keys = client.describe_key_pairs()['KeyPairs']
    if not next((key for key in customEC2Keys if key["KeyName"] == ec2_key_name), False):
        ec2_key_pair = ec2.create_key_pair(KeyName=ec2_key_name)
        print("New Private Key created,Save the below key-material\n\n")
        print(ec2_key_pair.key_material)
        return ec2_key_name

# Creating Instance 
def creating_instance (sec_ids, sub_ids, key_name):
    userDataCode = """
    #!/bin/bash
    set -e -x

    # Setting up the HTTP server 
    sudo yum install -y httpd 
    sudo service httpd start
    sudo chkconfig httpd on
    sudo groupadd www
    sudo usermod -a -G www ec2-user
    

    # Set the permissions
    sudo chown -R root:www /var/www
    sudo chmod 2775 /var/www
    sudo find /var/www -type d -exec chmod 2775 {} +
    sudo find /var/www -type f -exec chmod 0664 {} +

# SE Linux permissive
# needed to make wp connect to DB over newtork
# setsebool -P httpd_can_network_connect=1
# setsebool httpd_can_network_connect_db on

    sudo service httpd restart
    echo "AWS Automation is Fun!" | sudo tee /var/www/html/index.html
    """

    instances = ec2.create_instances(ImageId=ami_id,
                                       MinCount=1,
                                       MaxCount=1,
                                       KeyName=key_name,
                                       UserData=userDataCode,
                                       InstanceType=instance_type,
                                       BlockDeviceMappings=[
                                            {
                                                'DeviceName': '/dev/sdf',
                                                'Ebs': {

                                                'DeleteOnTermination': True,
                                                'VolumeSize': 1,
                                                'VolumeType': 'gp2'
                                            },
                                        },
                                       ],
                                       NetworkInterfaces=[
                                           {
                                               'SubnetId': sub_ids,
                                               'Groups': sec_ids,
                                               'DeviceIndex': 0,
                                               'DeleteOnTermination': True,
                                               'AssociatePublicIpAddress': True,
                                            }
                                         ]
                                         )
    ec2.create_tags(Tags=[{"Key": "Owner", "Value": "test"}], Resources=[instances[0].id])
    instance = instances[0]
# Wait for the instance to enter the running state
    print ( "Waiting for Instances to come up for Public DNS ")
    instance.wait_until_running()
# Reload the instance attributes
    instance.load()
    print(instance.public_dns_name)
    print("Wait about 1 minute and use the public dns for the web page.")
    
def main ():
    vpc_check()
    vpc_obj = create_vpc(cidr_block)
    sub_ids = create_subnets_list(subnets, az, vpc_obj)
    ig_id = create_internet_gateway(vpc_obj)
    update_route_table(ig_id, vpc_obj)
    sec_ids = creating_security_groups (vpc_obj)
    ec2_key_name = creating_key_pairs()
    creating_instance (sec_ids, sub_ids, ec2_key_name)
    

if __name__ == '__main__':
    main()
